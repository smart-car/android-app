package com.ssg.safecar;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "SAFE-CAR";
    private static final int REQUEST_ENABLE_BT = 1;
    private static final String NAME_DEVICE = "BTSC";
    //private static final String NAME_DEVICE = "Jorge Calle => MotoG3";
    UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    BluetoothAdapter bluetoothAdapter;
    BluetoothDevice bluetoothDevice;
    BluetoothSocket bluetoothSocket;
    InputStream inputStream;
    OutputStream outputStream;

    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    TextView textViewDeviceConnected;
    Button btnAddUser;
    Button btnRemoveUser;
    Button btnDoor;
    Button btnElectricity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textViewDeviceConnected = (TextView) findViewById(R.id.TextViewDeviceConnected);
        btnAddUser = (Button) findViewById(R.id.ButtonAddUser);
        btnRemoveUser = (Button) findViewById(R.id.ButtonRemoveUser);
        btnDoor = (Button) findViewById(R.id.ButtonDoor);
        btnElectricity = (Button) findViewById(R.id.ButtonElectricity);
        btnAddUser.setOnClickListener(clickAddUser);
        statusButtons("off");
        searchBluetooth();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {
            Log.i(TAG,"Rechazado por el usuario");
            searchBluetooth();
        }

        if (resultCode == REQUEST_ENABLE_BT){
            Log.i(TAG,"Activaste el bluetooth");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.ItemConnect) {
            connectDevice();
        }

        if (id == R.id.ItemDisconnect) {
            turnOffBluetooth();
        }

        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener clickAddUser = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            sendData("{'command':'eu'}");
        }
    };

    public void deviceNotSupportBluetooth () {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Dispositivo Incompatible")
                .setMessage("Lo siento, Tu celular no soporta Bluetooth :(")
                .setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void searchBluetooth () {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            deviceNotSupportBluetooth();
        }
        if (!bluetoothAdapter.isEnabled()){
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent,REQUEST_ENABLE_BT);
        }

        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() >0){
            for (BluetoothDevice device : pairedDevices) {
                Log.i(TAG,device.getName()+" => "+device.getAddress());
                if (device.getName().equals(NAME_DEVICE)) {
                    bluetoothDevice = device;
                    Log.e(TAG,bluetoothDevice.getName());
                    break;
                }
            }
        }
    }

    public void connectDevice() {
        try {
            bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(MY_UUID);
            bluetoothSocket.connect();
            inputStream = bluetoothSocket.getInputStream();
            outputStream = bluetoothSocket.getOutputStream();
            Toast.makeText(MainActivity.this,"Conexión Exitosa",Toast.LENGTH_SHORT).show();
            textViewDeviceConnected.setText(bluetoothDevice.getName());
            statusButtons("on");
            beginListenData();
        } catch (IOException e) {
            Log.e(TAG,"Ocurrió un error al conectar con el dispositivo");
            showDialogErrorToConnect();
        }
    }

    void beginListenData() {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character
        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable() {
            public void run() {
                while(!Thread.currentThread().isInterrupted() && !stopWorker) {
                    try {
                        int bytesAvailable = inputStream.available();
                        if(bytesAvailable > 0) {
                            byte[] packetBytes = new byte[bytesAvailable];
                            inputStream.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++) {
                                byte b = packetBytes[i];
                                if(b == delimiter) {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;
                                    handler.post(new Runnable() {
                                        public void run() {
                                            if(!data.isEmpty()){
                                                String btMSG = data.substring(0, data.length()-1);
                                                readData(btMSG);
                                            }
                                        }
                                    });
                                }
                                else {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (Exception ex) {
                        stopWorker = true;
                    }
                }
            }
        });

        workerThread.start();
    }

    public void readData(String data) {
        switch (data) {
            case "comando1":
                Log.i(TAG,data);
                break;

            case "comando2":
                Log.i(TAG,data);
                break;
        }
    }

    public void sendData(String data) {
        try {
            String msg = data;
            //msg += "\r\n";
            Log.i(TAG,data);
            outputStream.write(msg.getBytes());
            Toast.makeText(MainActivity.this, "se envio el mensaje al bluetooth", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Log.e(TAG,"Ocurrió un error al enviar un mensaje");
        }
    }

    public void statusButtons (String state) {

        switch (state) {
            case "on":
                btnAddUser.setEnabled(true);
                btnRemoveUser.setEnabled(true);
                btnRemoveUser.setEnabled(true);
                btnDoor.setEnabled(true);
                btnElectricity.setEnabled(true);
                break;
            case "off":
                btnAddUser.setEnabled(false);
                btnRemoveUser.setEnabled(false);
                btnRemoveUser.setEnabled(false);
                btnDoor.setEnabled(false);
                btnElectricity.setEnabled(false);
                break;
        }
    }

    public void turnOffBluetooth() {
        try {
            if (bluetoothSocket != null){
                bluetoothSocket.close();
                bluetoothSocket = null;
            }
            if (inputStream != null) {
                inputStream.close();
                inputStream = null;
            }
            if (outputStream != null) {
                outputStream.close();
                outputStream = null;
            }
            statusButtons("off");
            textViewDeviceConnected.setText("Sin conexión");

        } catch (IOException e) {
            Log.e(TAG,"Ocurrió un error al desconectar el dispositivo");
        }
    }

    public void showDialogErrorToConnect() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Dispositivo No Disponible");
        builder.setMessage("El dispositivo con el que se quiere conectar no esta disponible");
        //builder.setIcon(R.drawable.ic_bluetooth_disabled_black_36dp);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.i(TAG,"DISTE CLICK EN OK");
            }
        });
        builder.create();
        builder.show();
    }

}
